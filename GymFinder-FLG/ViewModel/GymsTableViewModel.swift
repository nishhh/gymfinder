//
//  GymsTableViewModel.swift
//  GymFinder-FLG
//
//  Created by Nishant Shrestha on 15/2/18.
//  Copyright © 2018 Nishant Shrestha. All rights reserved.
//

import Foundation

protocol GymsTableViewModelDelegate: class {
    func gymsTableViewModelSuccessfullyFetchedGyms(_ viewModel: GymsTableViewModel)
    func gymsTableViewModel(_ viewModel: GymsTableViewModel, encounteredError error: String)
}

class GymsTableViewModel {
    
    var service: GymService
    
    weak var delegate: GymsTableViewModelDelegate?
    
    private var gyms: [Gym] = [] {
        didSet {
            delegate?.gymsTableViewModelSuccessfullyFetchedGyms(self)
        }
    }
    
    private var errorMessage: String = "" {
        didSet {
            delegate?.gymsTableViewModel(self, encounteredError: errorMessage)
        }
    }
    
    init(service: GymService) {
        self.service = service
    }
    
    func fetchGyms(atLatitude latitude: Double, longitude: Double) {
        // Call the service to make API request
        service.fetchGyms(atLatitude: latitude, longitude: longitude) { [weak self] (result) in
            switch result {
            case .success(let gyms):
                self?.gyms = gyms
            case .error(let error):
                self?.errorMessage = error
            }
        }
    }
    
    func numberOfGyms() -> Int {
        return gyms.count
    }
    
    func gym(atIndex index: Int) -> Gym? {
        guard index < gyms.count else { return nil }
        
        return gyms[index]
    }
}
