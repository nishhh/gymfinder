//
//  GymService.swift
//  GymFinder-FLG
//
//  Created by Nishant Shrestha on 15/2/18.
//  Copyright © 2018 Nishant Shrestha. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class GymService {
    func fetchGyms(atLatitude latitude: Double, longitude: Double, completionBlock: @escaping (Result<[Gym]>) -> Void) {
        
        // Hardcode for now
        let url = "https://private-anon-664f13a09e-fitlgdemo.apiary-mock.com/api/v1/gyms"
        
        let parameters: [String: Any] = [
            "latitude": latitude,
            "longitude": longitude
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters)
            .validate()
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    // Validate that we got response from the API
                    guard let data = response.data else {
                        completionBlock(.error("Did not receive response from network. Please check your internet and try again."))
                        return
                    }
                    
                    // Validate that we got an array as the response
                    guard let gymArrayJSON = JSON(data: data).array else {
                        completionBlock(.error("Did not receive an array of results."))
                        return
                    }
                    
                    // Parse the JSON objects to model objects
                    let parsedGyms: [Gym] = gymArrayJSON.flatMap { Gym.init(fromJSON: $0) }
                    
                    // Successful completion block
                    completionBlock(.success(parsedGyms))
                case .failure(let error):
                    completionBlock(.error(error.localizedDescription))
                }
        }
    }
}
