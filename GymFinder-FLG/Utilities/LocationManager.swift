//
//  LocationManager.swift
//  GymFinder-FLG
//
//  Created by Nishant Shrestha on 15/2/18.
//  Copyright © 2018 Nishant Shrestha. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationManagerDelegate: class {
    func locationManagerdidRetrieveLocation(location: CLLocation)
    func locationManagerFailedBecauseOfReason(reason: String)
}

class LocationManager: NSObject {
    
    var locationManager: CLLocationManager
    
    weak var delegate: LocationManagerDelegate?
    
    init(locationManager: CLLocationManager = CLLocationManager()) {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        self.locationManager = locationManager
        
        // need to super init before being able to assign the delegate
        super.init()
        
        locationManager.delegate = self
    }
    
    func requestLocation() {
        locationManager.requestLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
        delegate?.locationManagerFailedBecauseOfReason(reason: error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            delegate?.locationManagerFailedBecauseOfReason(reason: "Could not get your location.")
            return
        }
        
        delegate?.locationManagerdidRetrieveLocation(location: location)
    }
}
