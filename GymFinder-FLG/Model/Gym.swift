//
//  Gym.swift
//  GymFinder-FLG
//
//  Created by Nishant Shrestha on 15/2/18.
//  Copyright © 2018 Nishant Shrestha. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Gym {
    let name: String
    let address: String
    
    // Default init
    init(name: String, address: String) {
        self.name = name
        self.address = address
    }
    
    // Failable initialiser
    init?(fromJSON json: JSON) {
        guard let name = json["name"].string,
            let address = json["address"].string,
            name.isEmpty == false,
            address.isEmpty == false else {
                return nil
        }
        
        self = Gym(name: name, address: address)
    }
}
