//
//  Result.swift
//  GymFinder-FLG
//
//  Created by Nishant Shrestha on 15/2/18.
//  Copyright © 2018 Nishant Shrestha. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case error(String)
}
