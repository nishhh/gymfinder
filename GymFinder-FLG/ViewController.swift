//
//  ViewController.swift
//  GymFinder-FLG
//
//  Created by Nishant Shrestha on 15/2/18.
//  Copyright © 2018 Nishant Shrestha. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var locationManager: LocationManager!
    var viewModel: GymsTableViewModel!
    
    let service = GymService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupViewModel()
        setupLocationManager()
        setupTableView()
    }
    
    private func setupLocationManager() {
        locationManager = LocationManager()
        locationManager.delegate = self
        locationManager.requestLocation()
    }
    
    private func setupViewModel() {
        viewModel = GymsTableViewModel(service: service)
        viewModel.delegate = self
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        // Remove separators
        tableView.tableFooterView = UIView()
        
        // Autoresizing cell
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
}

// MARK:- View model delegate
extension ViewController: GymsTableViewModelDelegate {
    func gymsTableViewModelSuccessfullyFetchedGyms(_ viewModel: GymsTableViewModel) {
        print("fetched data")
        // Refresh tableview
        tableView.reloadData()
    }
    
    func gymsTableViewModel(_ viewModel: GymsTableViewModel, encounteredError error: String) {
        showError(forType: .api, message: error)
    }
}

// MARK:- Location services
extension ViewController: LocationManagerDelegate {
    func locationManagerFailedBecauseOfReason(reason: String) {
        showError(forType: .location, message: reason)
    }
    
    func locationManagerdidRetrieveLocation(location: CLLocation) {
        print("retrieved location: ", location)
        
        // Fetch gym data when location retrieval is succesful
        viewModel.fetchGyms(atLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
    }
}

// MARK:- Tableview delegates
extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfGyms()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "gymCell") else {
            return UITableViewCell()
        }
        
        // Get the relevant model object from the tableViewModel
        let gym = viewModel.gym(atIndex: indexPath.row)
    
        // Usually would configureCell with a viewModel, but since the viewModel in this case wouldn't be doing any manipulation, I've left it as the model object.
        cell.textLabel?.text = gym?.name
        cell.detailTextLabel?.text = gym?.address
        
        return cell
    }
}

// MARK:- Error handling
extension ViewController {
    private func showError(forType type: ErrorType, withTitle title: String = "Gym Finder", message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        var action: UIAlertAction
        
        // The action is going to be different based on the error type
        if type == .location {
            action = UIAlertAction(title: "Try again", style: .default, handler: { [weak self](_) in
                self?.locationManager.requestLocation()
            })
        } else {
            action = UIAlertAction(title: "OK", style: .default, handler: nil)
        }
        
        alertController.addAction(action)
        
        present(alertController, animated: true, completion: nil)
    }
}

