//
//  GymTests.swift
//  GymFinder-FLGTests
//
//  Created by Nishant Shrestha on 15/2/18.
//  Copyright © 2018 Nishant Shrestha. All rights reserved.
//

import Foundation
import Quick
import Nimble
import SwiftyJSON
@testable import GymFinder_FLG

class GymTests: QuickSpec {
    override func spec() {
        describe("GymModel") {
            context("With Invalid Name and Address", closure: {
                
                let invalidDictionary: Dictionary<String, Any> = [
                    "name": "",
                    "address": ""
                ]
                
                let invalidJSON = JSON(invalidDictionary)
                let gym = Gym(fromJSON: invalidJSON)
                
                it("should not be initalised", closure: {
                    expect(gym).to(beNil())
                })
            })
            
            context("With Valid Name and Address", closure: {
                
                let validDictionary: Dictionary<String, Any> = [
                    "name": "My Amazing Gym",
                    "address": "123 Street Address, State"
                ]
                
                let validJSON = JSON(validDictionary)
                let gym = Gym(fromJSON: validJSON)
                
                it("should be initalised with correct values", closure: {
                    expect(gym).toNot(beNil())
                    expect(gym?.name).to(equal("My Amazing Gym"))
                    expect(gym?.address).to(equal("123 Street Address, State"))
                })
            })
        }
    }
}
